var express   =    require("express");
var mysql     =    require('mysql');
var app       =    express();

var pool      =    mysql.createPool({
    connectionLimit : 100, //important
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'ecoupon',
    debug    :  false
});

function get_all_company(req,res) {    
    pool.getConnection(function(err,connection){
        if (err) {
          connection.release();
          res.json({"code" : 100, "status" : "Error in connection database"});
          return;
        }        
        connection.query("select * from company", function(err,rows){
            connection.release();
            if(!err) {
                res.json(rows);
            }           
        });
        connection.on('error', function(err) {      
              res.json({"code" : 100, "status" : "Error in connection database"});
              return;     
        });
  });
}

function get_company_by_id(req,res) {    
    pool.getConnection(function(err,connection){
        if (err) {
          connection.release();
          res.json({"code" : 100, "status" : "Error in connection database"});
          return;
        }    
		var id = req.query.id;
		console.log('companyid:' + id);	
        connection.query("select * from company where company_id=?",[id], function(err,rows){
            connection.release();
            if(!err) {
                res.json(rows);
            }           
        });
        connection.on('error', function(err) {      
              res.json({"code" : 100, "status" : "Error in connection database"});
              return;     
        });
  });
}

//http://localhost:3000	
app.get("/",function(req,res){
    get_all_company(req,res);
});

//http://localhost:3000/company?id=2	
app.get("/company", function(req, res) {
	get_company_by_id(req,res);
	
});



app.listen(3000);